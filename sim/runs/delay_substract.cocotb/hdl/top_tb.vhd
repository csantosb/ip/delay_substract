-- * Top
--
--! @file
--! @brief Topmost hierarchy level.
--!
--! Its solely purpose is to wrap the averager module, being used as topmost
--! level under xilinx ise project manager.
--!
--! It declares input signals, define their size and instantiate the averager
--! module.
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...
--!
--! \class top

-- * Libraries

library ieee;
use ieee.std_logic_1164.all;
--! Project package\n\n
use work.top_pkg.all;
use IEEE.numeric_std.all;

-- * Entity
--
--! @brief Top entity.
--!
--! @details Rst and clk have standard functionality.\n
--! Samples is a fixed number of bits input data bus.\n\n
--!
--! Average is the sum of all samples in the window, divided by its length,
--! thus the average.\n
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...

entity top_tb is
  port (clk     : in  std_logic;
        rst     : in  std_logic;
        samples : in  std_logic_vector(c_width-1 downto 0);
        output  : out std_logic_vector(c_width downto 0));
end entity top_tb;

-- * Architecture

--! @brief Just instantiate the averager module.
--!
--! @details Details of the architecture.\n
--! Simpler that this, you die !.\n
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...

architecture simpler of top_tb is

  -- ** signals

  signal s_samples : signed(samples'range)           := (others => '0');
  signal s_output  : signed(samples'left+1 downto 0) := (others => '0');

begin

  -- ** averager

  --! @brief Implements a simple, general purpose moving averaging.
  --!
  --! @details It delays the incoming sampling signals, sampling them in parallel.\n
  --! Then, takes the difference of the two parallel streams\n
  --! accumulating the difference.\n
  --! Finally, redimensions the results to compute the division by the window
  --! lenght
  --!
  --! @section sec1 Section1
  --!
  --! All is clear, I guess ...
  --!
  --! @section sec2 Section2
  --!
  --! Don’t know what to say ...

  s_samples <= signed(samples);
  output    <= std_logic_vector(s_output);

  DUT : entity work.delay_substract
    generic map (g_delay => c_delay)
    port map (clk    => clk,
              rst    => rst,
              input  => s_samples,
              output => s_output);

end architecture simpler;
